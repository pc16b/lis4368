> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**



## Paula Castanos 

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide Screenshots of installations
    - Create Bitbucket tutorial (bitbucketlocations and myteamquotes)
    - Provide git command descriptions


2. [A2 README.md](a2/README.md "My A2 README.md file")
   
     Display the following links: 
     - http://localhost:9999/hello 

     - http://localhost:9999/hello/HelloHome.html (Rename"HelloHome.html" to "index.html"so that users cannot see your files!) -> http://localhost:9999/hello/index.html

     - http://localhost:9999/hello/sayhello  (invokes HelloServlet)Note: /sayhello maps to HelloServlet.class(changed web.xml file)

     - http://localhost:9999/hello/querybook.html 

3. [A3 README.md](a3/README.md "My A3 README.md file")
    Deliverables: 
    - Entity Relationship Diagram (ERD) 
       - ![A3ERD](a3/img/a3.png)
    - Include data (at least 10 records each table)
    - Provide Bitbucket read-only access to lis4368 repo (Language SQL)
    - Blackboard Links: lis4368 Bitbucket rep

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Blackboard Links: lis 4368 repo
    - Provide Bitbucket read-only acess to lis4368 repo, including links to other assignements 
    - Carousel must contain min of 3 slides 

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Basic server-side validation 
    - Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos
    - Blackboard Links: lis4368 repo
  

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Basic server-side validation with database entry
    - Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos
    - Blackboard Links: lis4368 repo

7. [A7 README.md](a7/README.md "My A7 README.md file")
 - TBD