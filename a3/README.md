> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 Advanced Web App Development 

## Paula Castanos

### Assignment 3 Requirements:
Assignment 3 link - http://localhost:9999/lis4368/a3/

Deliverables:

1. Entity Relationship Diagram
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to lis4368 repo (LanguageSQL), must include README.md using Markdown syntax, and include links to all the following files (from README.md);
 - docs folder: a3.mwb and a3.sql
 - img folder: a3.png (export a3.mwb file as a3.png)
 - README.md (MUST display a3.png ERD)
4. Blackboard links: lis4368 rep



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots and Links: 

*Screenshot of A3 ERD:*

![A3ERD](img/a3.png)


*A3 Docs: a3.mwb and a3.sql*;

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 ERD in .sql format")



