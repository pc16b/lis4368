> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 Advanced Web App Development 

## Paula Castanos

### Assignment Project 1 Requirements:
Project 1 link - http://localhost:9999/lis4368/p1/ 

Deliverables:

1. Provide Bitbucket read-only access to lis4368 repo include links
2. README.md
3. Blackboard links: lis4368 rep
4. Carousel must contain 3 slides



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots and Links: 

*Screenshot of failed validation:*

![P1](img/fv1.png)
![P1](img/fv2.png)



*Screenshot of client-side validation:*

![P1](img/validation.png)

