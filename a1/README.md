<!--> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
> -->

# LIS 4368

## Paula Castanos

### Assignment #1 Requirements:

*Three Ports:*

1. Distributed Version Control with Git
2. Java/SP/Servlet Development Installation
3. Chapter Questions

#### README.md file should include the following items:

* Screenshot of running Java Hello #1 above
* Screenshot of running http://localhost:9999/lis4368/a1
* git commands with short descriptions
* Bitbucket repo links: a) this assignment b) completed tutorial above (bitbucket stationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - definition goes here 
2. git status 
3. git add
4. git commit
5. git push
6. git pull
7. One additional git command

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
