> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 Advanced Web App Development 

## Paula Castanos

### Assignment 2 Requirements:
Assignment 2 link - http://localhost:9999/lis4368/a2/

*Three Ports:*

1. Servlet Compilation and Usage
2. Database Connectivity Using Servlets
3. Chapter Questions 

#### README.md file should include the following items:

* Assessment links
* Only *one* screenshot of the query results from the following link (see screenshot below): http://localhost:9999/hello/querybook.html


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - definition goes here
2. git status
3. git add
4. git commit
5. git push
6. git pull
7. One additional git command 

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello/*:

![/hello/ Screenshot](img/hello.png)

*Screenshot of http://localhost:9999/hello/index.html*:

![/hello/index.html Screenshot](img/helloindexhtml.png)

*Screenshot of http://localhost:9999/hello/sayhello*:

![/sayhello Screenshot](img/sayhello.png)

*Screenshot of http://localhost:9999/hello/querybook.html*:

![/hello/querybook.html Screenshot](img/querybookhtml.png)

*Screenshot of Query Results*:

![Query Results Screenshot](img/queryresults.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")