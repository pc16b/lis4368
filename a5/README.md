> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development 

## Paula Castanos

### Assignment 5 Requirements:
Assignment 5 link - http://localhost:9999/lis4368/customerform.jsp?assign_num=a5

Deliverables:

1. Basic Server-side validation with database entry
2. Provide Bitbucket read-only access to lis4368 repo (LanguageSQL), must include README.md using Markdown syntax
4. Blackboard links: lis4368 rep



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots: 

*Screenshot of failed basic server-side form entry*

![A5Validation](img/formentry.png)

*Screenshot of error in basic server-side validation*

![A5Validation](img/validation.png)


*Screenshot of passed basic server-side database entry*

![A5Validation](img/databaseentry.png)



