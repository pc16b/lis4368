> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development 

## Paula Castanos

### Assignment 4 Requirements:
Assignment 4 link - http://localhost:9999/lis4368/customerform.jsp?assign_num=a4 

Deliverables:

1. Basic Server-side validation - failed and passed include screenshots
2. Provide Bitbucket read-only access to lis4368 repo (LanguageSQL), must include README.md using Markdown syntax
4. Blackboard links: lis4368 rep



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots: 

*Screenshot of failed basic server-side validation*

![A4Validation](img/fvalidation.png)

*Screenshot of error in basic server-side validation*

![A4Validation](img/error.png)


*Screenshot of passed basic server-side validation*

![A4Validation](img/pvalidation.png)



